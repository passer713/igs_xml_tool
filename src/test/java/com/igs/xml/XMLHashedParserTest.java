package com.igs.xml;

import static org.junit.Assert.assertEquals;

/**
 * Created by elmar on 17/03/16.
 */
public class XMLHashedParserTest {

    @org.junit.Test
    public void testParseXmlString() throws Exception {
        XML_Data dt = XMLHashedParser.parse("src/test/resources/test.xml");

        assertEquals(5, dt.getChildByName("catalog").getChildCount());
        assertEquals("book", dt.getChildByName("catalog").getChilds()[0].getName());
        assertEquals(3, dt.getChildByName("catalog").getChildsAsArray("book").length);
        assertEquals(2, dt.getChildByName("catalog").getChildsAsArray("dictionary").length);

        assertEquals(true, dt.getChildByName("catalog").getChildByName("book", 0).hasChildByName("price"));
        assertEquals(false, dt.getChildByName("catalog").getChildByName("book", 0).hasChildByName("price",2));
        assertEquals(false, dt.getChildByName("catalog").getChildByName("book", 0).hasChildByName("newPrice"));

        assertEquals(false, dt.getChildByName("catalog").getChildByName("book", 0).getChildByName("price").isEmpty());
        assertEquals(true, dt.getChildByName("catalog").getChildByName("book", 0).getChildByName("price", 2).isEmpty());
        assertEquals(true, dt.getChildByName("catalog").getChildByName("book", 0).getChildByName("newPrice").isEmpty());

        assertEquals("44.95", dt.getChildByName("catalog").getChildByName("book", 0).getChildByName("price").getValue());
        assertEquals("5.95", dt.getChildByName("catalog").getChildByName("book", 1).getChildByName("price").getValue());
        assertEquals("bk101", dt.getChildByName("catalog").getChildByName("book", 0).getAttributeByName("id"));
        assertEquals("bk102", dt.getChildByName("catalog").getChildByName("book", 1).getAttributeByName("id"));
        assertEquals("dic2", dt.getChildByName("catalog").getChildByName("dictionary", 1).getAttributeByName("id"));

        assertEquals("5.95", dt.getChild("catalog.book(1).price").getValue());
    }

    @org.junit.Test
    public void testParse() throws Exception {

    }
}