package com.igs.xml;

import java.io.Serializable;

public class MyAttribute implements Serializable {
	private String name;
	private String value;

	public static MyAttribute createAttribute(String name,String value){
		MyAttribute a=new MyAttribute();
		a.name=name;
		a.value=value;
		return a;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}
}
