package com.igs.xml;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

public class XMLParser extends DefaultHandler implements Serializable {
	public xml_record[] data;
	public StringBuffer activepos;
	public int len=0;


	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		activepos.append(qName).append(".");
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		activepos.setLength(activepos.length()-1);
		activepos.setLength(activepos.lastIndexOf(".")+1);
	}

	public void characters(char ch[], int start, int length) throws SAXException {
		String val=(new String(ch,start, length)).trim();
		if (val.equals(""))return;
		String capt=(activepos.substring(0,activepos.length()-1)).toLowerCase();

		int wil_id=0;
		int last=-1;
		for(int i=0;i<len;i++)
			if (data[i]!=null && data[i].caption.equals(capt)){
				wil_id++;
				last=i;
			}

		xml_record r=new xml_record();
		r.pos=wil_id;
		r.caption=capt;
		r.value=val;
		if(last!=-1){
			r.prev=last;
			data[last].next=len;
		}

		data[len]=r;
		len++;
	}

	public XMLParser() {
		this.data = new xml_record[20000];
		activepos=new StringBuffer();
	}

	public void parse(String XMLfile) throws ParserConfigurationException, SAXException, IOException {
		parse(new InputSource(new FileInputStream(XMLfile)));
	}
	public void parse(InputSource is) throws ParserConfigurationException, SAXException, IOException {
		XMLReader myparser = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
		myparser.setContentHandler(this);
		myparser.parse(is);

		xml_record[] dt=new xml_record[len];
		for (int i=0;i<len;i++)
			dt[i]=data[i];
		data=dt;
	}

	public String getval(String cap){
		return getval(cap,0);
	}

	public String getval(String cap,int id){
		for (int i=0;i<len;i++)
			if (data[i].caption.equals(cap)){
				int rid = i;
				while(id>0 && (rid=data[rid].next)!=-1)
					id--;
				if(rid==-1)break;
			   return data[rid].value;
			}
		return "";
	}
}
