package com.igs.xml;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;

/**
 * Class for parsing xml documents.
 * <p/>
 * {@code
 * example:
 * XML_Data data = XMLHashedParser.parse(XML_File).getChildByName("service").getChildByName("database");
 * StringBuffer ConnString = new StringBuffer();
 * ConnString.append(data.getChildByName("host").getValue()).append("?user=").append(data.getChildByName("user").getValue()).append("&password=").append(data.getChildByName("password").getValue());
 * return ConnString.toString();
 * }
 * <p/>
 *
 *
 * User: elmar
 * Date: May 11, 2008
 */
public class XMLHashedParser extends DefaultHandler implements Serializable {
	public XML_Data data;
	private XML_Data realdata;
	StringBuffer CurrData;

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		CurrData = new StringBuffer();

		XML_Data tmp = new XML_Data();
		tmp.setName(qName);
		tmp.setAttributes(attributes);
		realdata.AddChilds(tmp);
		realdata = tmp;
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		realdata.setValue(CurrData.toString().trim());
		CurrData = new StringBuffer();
		realdata.optimize();
		realdata = realdata.getParent();
	}

	public void characters(char ch[], int start, int length) throws SAXException {
		CurrData.append(ch, start, length);
	}


	private XMLHashedParser() {
		data = new XML_Data();
		data.setName("root");
		data.setValue("");
		realdata = data;
	}

	/**
	 * parsing xml String
	 *
	 * @param XmlString
	 * @return parsed xml data
	 * @throws ParserConfigurationException
	 * @throws SAXException				 happen if xml cannot be parsed
	 * @throws IOException
	 */
	public static XML_Data parseXmlString(String XmlString) throws ParserConfigurationException, SAXException, IOException {
		InputSource is = new InputSource(new StringReader(XmlString));
		is.setEncoding("UTF-8");
		return parse(is);
	}

	/**
	 * parsing xml file/
	 *
	 * @param XMLfile address
	 * @return parsed xml data
	 * @throws ParserConfigurationException
	 * @throws SAXException				 happen if xml cannot be parsed
	 * @throws IOException				  happen if there any problem with address of file
	 */
	public static XML_Data parse(String XMLfile) throws ParserConfigurationException, SAXException, IOException {
		return parse(new InputSource(new FileInputStream(XMLfile)));
	}

	/**
	 * parsing xml from InputSource
	 *
	 * @param XMLFileSource input sourse
	 * @return parsed xml data
	 * @throws ParserConfigurationException
	 * @throws SAXException				 happen if xml cannot be parsed
	 * @throws IOException				  happen if there any problem with InputSource
	 */
	public static XML_Data parse(InputSource XMLFileSource) throws ParserConfigurationException, SAXException, IOException {
		XMLHashedParser parser = new XMLHashedParser();
		XMLReader myparser = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
		myparser.setContentHandler(parser);
		myparser.parse(XMLFileSource);
		parser.data.optimize();
		return parser.data;
	}

	/**
	 * parse mysql connection xml file
	 *
	 * @param XML_File connection file address
	 * @return connection string
	 * @throws IOException				  happen if there any problem with address of file
	 * @throws ParserConfigurationException
	 * @throws SAXException				 happen if xml cannot be parsed
	 */
	public static String CreateConnection(String XML_File) throws IOException, ParserConfigurationException, SAXException {
		XML_Data data = XMLHashedParser.parse(XML_File).getChildByName("service").getChildByName("database");

		StringBuffer ConnString = new StringBuffer();
		ConnString.append(data.getChildByName("host").getValue()).append("?user=").append(data.getChildByName("user").getValue()).append("&password=").append(data.getChildByName("password").getValue());
		return ConnString.toString();
	}

	public static void main(String[] args) {
		try {
			XML_Data x = XMLHashedParser.parseXmlString("<AWRequest>\n" +
					"\t<MessageHead>\n" +
					"\t\t<Term>GoldenPay</Term>\n" +
					"\t\t<User>User</User>\n" +
					"\t\t<RequestDate>2012-07-04T16:36:01</RequestDate>\n" +
					"\t\t<Type>Check</Type>\n" +
					"\t\t<SessionId>GPS1568005</SessionId>\n" +
					"\t\t<Language>az</Language>\n" +
					"\t\t<VendorId>Azerigaz</VendorId>\n" +
					"\t\t<PaymentType>Goldenpaysmart</PaymentType>\n" +
					"\t\t<PaymentKind>AGSmartCard</PaymentKind>\n" +
					"\t</MessageHead>\n" +
					"\t<MessageBody>\n" +
					"\t\t<KindAttribute>\n" +
					"\t\t\t<Name>SERIALNUM</Name>\n" +
					"\t\t\t<Value><![CDATA[0067b91a00250400]]></Value>\n" +
					"\t\t</KindAttribute>\n" +
					"\t\t<KindAttribute>\n" +
					"\t\t\t<Name>CardType</Name>\n" +
					"\t\t\t<Value><![CDATA[000016e9b82fe3d5000000000000]]></Value>\n" +
					"\t\t</KindAttribute>\n" +
					"\t\t<KindAttribute>\n" +
					"\t\t\t<Name>In</Name>\n" +
					"\t\t\t<Value>\n" +
					"\t\t\t\t<![CDATA[2791fa90170100003c05df4f4e0001015c00380005019400060001029a00040001039e000e000203ac001c000303c80024000104ec0006001104f20004002c04f60002002b04f80006000404fe0002001b0400010000ffff0001000000000000000000000000000000000000000000004630303030303030303331383436390000000000860700004750473132303736343130010000000000003200000080436d380101e8030001010001010000000001000000000000000000000000000000000000000000000001010101c0070000000000000000000000000000000000000000000000000000000001000000000001006400fe00000080436d386400e8030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000]]></Value>\n" +
					"\t\t</KindAttribute>\n" +
					"\t\t<KindAttribute>\n" +
					"\t\t\t<Name>Out</Name>\n" +
					"\t\t\t<Value>\n" +
					"\t\t\t\t<![CDATA[2791fa9017010000ab99df4f000002021a000400ffff1e00000032000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000]]></Value>\n" +
					"\t\t</KindAttribute>\n" +
					"\t\t<KindAttribute>\n" +
					"\t\t\t<Name>Extra</Name>\n" +
					"\t\t\t<Value>\n" +
					"\t\t\t\t<![CDATA[6af7750416f600003c05df4f0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000]]></Value>\n" +
					"\t\t</KindAttribute>\n" +
					"\t\t<TypeAttribute>\n" +
					"\t\t\t<Name>Card</Name>\n" +
					"\t\t\t<Value>******1234</Value>\n" +
					"\t\t</TypeAttribute>\n" +
					"\t\t<TypeAttribute>\n" +
					"\t\t\t<Name>ExpireDate</Name>\n" +
					"\t\t\t<Value>07/10</Value>\n" +
					"\t\t</TypeAttribute>\n" +
					"\t\t<TypeAttribute>\n" +
					"\t\t\t<Name>Transaction</Name>\n" +
					"\t\t\t<Value>transid</Value>\n" +
					"\t\t</TypeAttribute>\n" +
					"\t\t<AmountAll>100</AmountAll>\n" +
					"\t\t<Amount>100</Amount>\n" +
					"\t\t<Currency>AZN</Currency>\n" +
					"\t</MessageBody>\n" +
					"</AWRequest>");
			System.out.println("x=" + x.getChildByName("AWRequest").toString());
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
